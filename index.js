// fetch()
/* 
    - this is a method in JS that is used to send request in the server and load the information (response from the server) in the webpages.
    Syntax:
        fetch("urlAPI", {optional/request from the user and response to the user})
*/

// API

/* 
let specificPost = (id) => {
    if (id <= 100 && id >= 1) {
        fetch(`https://jsonplaceholder.typicode.com/posts/${id}`)
            .then((response) => response.json())
            .then((data) => console.log(data));
    }
};

specificPost(5);
 */

let fetchPosts = () => {
    fetch(`https://jsonplaceholder.typicode.com/posts`)
        .then((response) => response.json())
        .then((data) => {
            console.log(data);
            return showPosts(data);
        });
};

fetchPosts();

const showPosts = (posts) => {
    let postEntries = "";

    posts.forEach((post) => {
        postEntries += `
        <div id="post-${post.id}">
            <h3 id="post-title-${post.id}">${post.title}</h3>
            <p id="post-body-${post.id}">${post.body}</p>
            <button onclick="editPost('${post.id}')">Edit</button>
            <button onclick="deletePost('${post.id}')">Delete</button>
        </div>
        `;
    });
    document.querySelector(`#div-post-entries`).innerHTML = postEntries;
};

// Add Post

document.querySelector(`#form-add-post`).addEventListener("submit", (event) => {
    event.preventDefault();

    let title = document.querySelector(`#txt-title`).value;
    let body = document.querySelector(`#txt-body`).value;

    fetch(`https://jsonplaceholder.typicode.com/posts`, {
        method: "POST",
        body: JSON.stringify({
            title: title,
            body: body,
            userId: 1,
        }),
        headers: {
            "Content-Type": "application/json",
        },
    })
        .then((response) => response.json())
        .then((data) => {
            console.log(data);
            alert(`Successfully added`);

            document.querySelector(`#txt-title`).value = null;
            document.querySelector(`#txt-body`).value = null;
        });
});

// Edit post
const editPost = (id) => {
    let title = document.querySelector(`#post-title-${id}`).innerHTML;
    let body = document.querySelector(`#post-body-${id}`).innerHTML;

    document.querySelector(`#txt-edit-title`).value = title;
    document.querySelector(`#txt-edit-body`).value = body;
    document.querySelector(`#txt-edit-id`).value = id;
    document.querySelector(`#btn-submit-update`).removeAttribute(`disabled`);
};

document
    .querySelector(`#form-edit-post`)
    .addEventListener("submit", (event) => {
        event.preventDefault();
        let id = document.querySelector(`#txt-edit-id`).value;
        let title = document.querySelector(`#txt-edit-title`).value;
        let body = document.querySelector(`#txt-edit-body`).value;

        fetch(`https://jsonplaceholder.typicode.com/posts/${id}`, {
            method: "PATCH",
            body: JSON.stringify({
                title: title,
                body: body,
            }),
            headers: {
                "Content-Type": "application/json",
            },
        })
            .then((response) => response.json())
            .then((data) => {
                console.log(data);
                alert(`Updated successfully`);

                document.querySelector(`#txt-edit-id`).value = null;
                document.querySelector(`#txt-edit-title`).value = null;
                document.querySelector(`#txt-edit-body`).value = null;
                document
                    .querySelector(`#btn-submit-update`)
                    .setAttribute(`disabled`, true);
            });
    });

// Delete post
const deletePost = async (id) => {
    await fetch(`https://jsonplaceholder.typicode.com/posts/${id}`, {
        method: "DELETE",
    })
        .then((response) => response.json())
        .then((data) => {
            console.log(data);
            alert(`Deleted successfully`);
        });

        document.querySelector(`#post-${id}`).remove();
};
